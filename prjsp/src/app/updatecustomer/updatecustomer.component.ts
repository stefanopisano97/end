import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-updatecustomer',
  templateUrl: './updatecustomer.component.html',
  styleUrls: ['./updatecustomer.component.css']
})
export class UpdatecustomerComponent implements OnInit {
  comune: any = {
    nome: "",
  }
  customer: any = {
    ragioneSociale: "",
    partitaIva: "",
    tipoCliente: "SRL",
    email: "",
    pec: "",
    telefono: "",
    nomeContatto: "",
    cognomeContatto: "",
    telefonoContatto: "",
    emailContatto: "",
    indirizzoSedeOperativa: {
      via: "",
      civico: "",
      cap: "",
      localita: "",
      comune: {
        id: 1,
        nome: "",
        provincia: {
          id: 1,
          nome: "",
          sigla: ""
        }
      }
    },
    indirizzoSedeLegale: {
      via: "",
      civico: "",
      cap: "",
      localita: "",
      comune: {
        id: 1,
        nome: "LASTRA A SIGNA",
        provincia: {
          id: 1,
          nome: "FIRENZE",
          sigla: "FI"
        }
      }
    },
    dataInserimento: "",
    dataUltimoContatto: ""
  }

  constructor(private router: Router, private route: ActivatedRoute, private userservice: UserService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => this.userservice.getCustomerId(params.id).subscribe(customer => this.customer = customer))
  }
  saveCustomer() {
    this.userservice.updateCustomer(this.customer).subscribe(resp => console.log(resp));
    this.router.navigate(['customers'])
    console.log(this.customer.id)
  }
}
