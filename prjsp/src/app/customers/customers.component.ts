import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {
  customers:any;

  constructor(private userservice: UserService, private modalService: NgbModal,private router: Router ) { }


  ngOnInit(): void {
    this.userservice.getAllCustomerspag(0).subscribe(data => this.customers = data);
  }

  getPaginations(pagination:number):void {
    this.userservice.getAllCustomerspag(pagination).subscribe(data => this.customers = data);
  }
  deleteCustomer(item: any) {
    /* alert('Customer Deleted!') */
    this.userservice.delCustomer(item.id).subscribe(resp => console.log(resp));
    this.customers = this.customers.filter((customer: { id: any; }) => customer.id !== item.id);
  }
  updateCustomer(item:any){
    /* alert('ok') */
    this.router.navigate(['customers', item.id, 'edit'])
  }
  }


