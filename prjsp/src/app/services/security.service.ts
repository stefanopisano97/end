import { Injectable } from '@angular/core';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  private login = false;

  constructor() { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    return this.login;
  }

  loginUtente(username: string, password: string) {
    if(username === '' && password === '') {
      this.login = true;
      alert('Welcome Back!')
    } else {
      alert('The Username and the Password were incorrect. Please, retry');
    }
  }
  logOut() {
    this.login = false;
    alert('Disconnected');
  }
}
