import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  headers = new HttpHeaders();
  beaererAuth =
  'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImlhdCI6MTYyODQ5NzgyNSwiZXhwIjoxNjI5MzYxODI1fQ.GIsSurt4oE1xTkH-8S-hdCg8GSG6gJ80tr-_pstcOkbUutFPwiDOjVVWFWcDqle1aBCFWHw4Hl_YZo03j0gtmg';

  constructor(private http: HttpClient) {
    /* this.headers = this.headers
      .set('Authorization', 'Bearer ' + this.beaererAuth)
      .set('X-TENANT-ID', 'fe_0321'); */
  }
  getAllCustomerspag(pagination:number) {
    return this.http.get<any>(environment.urlAPI + '/api/clienti?page=' + pagination + '&size=20&sort=id,DESC');
  }

  getCustomerId(id:number) {
return this.http.get<any>(environment.urlAPI + '/api/clienti/'+id)
  }
  delCustomer(id:number) {
    return this.http.delete<any>(environment.urlAPI + '/api/clienti/'+id)
  }
  addCustomer(customer: any) {
    return this.http.post<any>(environment.urlAPI + '/api/clienti/', customer)
  }
  getComuni(){
    return this.http.get<any>(environment.urlAPI + '/api/comuni?page=0&size=20&sort=id,ASC')
  }
  updateCustomer(customer:any){
    return this.http.put<any>(environment.urlAPI + '/api/clienti/'+customer.id, customer)
  }
  /****************FATTURE************************ */
  getFatture(){
    return this.http.get<any>(environment.urlAPI + '/api/fatture/')
  }

  removeFatture(id:number){
    return this.http.delete<any>(environment.urlAPI + '/api/fatture/'+id)
  }
  createFatture(fattura:any){
return this.http.post<any>(environment.urlAPI + '/api/fatture/', fattura)
  }
  /*********SIGN-IN************************************** */

  postRegistrazione(utente:any){
    return this.http.post<any>(environment.urlAPI + '/api/auth/signup', utente)
  }
}
