import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-bills',
  templateUrl: './bills.component.html',
  styleUrls: ['./bills.component.css']
})
export class BillsComponent implements OnInit {
  fatture: any

  constructor(private userservice: UserService, private router: Router, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.userservice.getFatture().subscribe(resp => this.fatture = resp)
  }

  getFatture(){
    this.fatture = this.userservice.getFatture();
  }
  removeFatture(item:any){
this.userservice.removeFatture(item.id).subscribe(resp => console.log(resp));
this.fatture= this.fatture.filter((fattura: { id: any; }) => fattura.id !== item.id);
alert('Bills Delete')
  }
}
