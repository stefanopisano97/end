import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-billscreate',
  templateUrl: './billscreate.component.html',
  styleUrls: ['./billscreate.component.css']
})
export class BillscreateComponent implements OnInit {
fattura: any = {data:"",numero:"",anno:"",importo:"",stato:{
  id: 1,
  nome: "PAGATA"
}
,cliente:{"id":1}}

  constructor(private router: Router, private userservice: UserService) { }

  ngOnInit(): void {

  }

  saveBill(){
    this.userservice.createFatture(this.fattura).subscribe(resp=> console.log(resp));
    this.router.navigate(['bills'])
    /* alert('ok') */
  }

}
