import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BillscreateComponent } from './billscreate.component';

describe('BillscreateComponent', () => {
  let component: BillscreateComponent;
  let fixture: ComponentFixture<BillscreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillscreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillscreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
