import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppCardsComponent } from './app-cards/app-cards.component';
import { BillsComponent } from './bills/bills.component';
import { BillscreateComponent } from './billscreate/billscreate.component';
import { CustomersComponent } from './customers/customers.component';
import { CustomerscreateComponent } from './customerscreate/customerscreate.component';
import { LoginComponent } from './login/login.component';
import { SecurityService } from './services/security.service';
import { SignupComponent } from './signup/signup.component';
import { UpdatecustomerComponent } from './updatecustomer/updatecustomer.component';

const routes: Routes = [
  {path:'', pathMatch:'full', redirectTo:'login'},
  {path:'home', component:AppCardsComponent , canActivate: [SecurityService] },
  {path: 'bills', component: BillsComponent , canActivate: [SecurityService] },
  {path: 'customers', component: CustomersComponent , canActivate: [SecurityService] },
  {path: 'bcreate', component: BillscreateComponent, canActivate: [SecurityService] },
  {path: 'customerscreate', component: CustomerscreateComponent , canActivate: [SecurityService] },
  {path:'customers/:id/edit', component: UpdatecustomerComponent , canActivate: [SecurityService] },
  {path: 'signin', component: SignupComponent},
  {path: 'login', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
