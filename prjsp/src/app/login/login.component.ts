import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SecurityService } from './../services/security.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username = '';
  password = '';
  hide = true

  constructor(
    private  secservice: SecurityService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }
  loginUser() {
    this.secservice.loginUtente(this.username, this.password);
    this.router.navigate(['/home']);
  }
}
