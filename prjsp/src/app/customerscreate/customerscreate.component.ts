import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';



@Component({
  selector: 'app-customerscreate',
  templateUrl: './customerscreate.component.html',
  styleUrls: ['./customerscreate.component.css']
})
export class CustomerscreateComponent implements OnInit {
  comune: any = {
    nome: "",
  }
customer: any = {
    ragioneSociale: "",
    partitaIva: "",
    tipoCliente: "SRL",
    email: "",
    pec: "",
    telefono: "",
    nomeContatto: "",
    cognomeContatto: "",
    telefonoContatto: "",
    emailContatto: "",
    indirizzoSedeOperativa: {
        via: "",
        civico: "",
        cap: "",
        localita: "",
        comune: {
            id: 1,
            nome: "",
            provincia: {
                id: 1,
                nome: "",
                sigla: ""
            }
        }
    },
    indirizzoSedeLegale: {
        via: "",
        civico: "",
        cap: "",
        localita: "",
        comune: {
            id: 1,
            nome: "LASTRA A SIGNA",
            provincia: {
                id: 1,
                nome: "FIRENZE",
                sigla: "FI"
            }
        }
    },
    dataInserimento: "",
    dataUltimoContatto: ""
}

  constructor(private router: Router , private userservice: UserService) { }

  ngOnInit(): void {
    this.userservice.getComuni().subscribe(data => this.comune = data);
  }

  saveCustomer() {
    this.userservice.addCustomer(this.customer).subscribe(resp => console.log(resp));
    alert('Customer Added')
    this.router.navigate(['customers'])
  }
}


