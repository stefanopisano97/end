import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerscreateComponent } from './customerscreate.component';

describe('CustomerscreateComponent', () => {
  let component: CustomerscreateComponent;
  let fixture: ComponentFixture<CustomerscreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerscreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerscreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
