import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  utente: any = {
    username: "",
    email: "",
    password: "",
    role: ["user"]
}
  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  signMeIn(){
    this.userService.postRegistrazione(this.utente).subscribe((response)=> {alert('Registrazione avvenuta correttamente'+ JSON.stringify(response))
  });
  this.router.navigate(['home'])
  }
}
